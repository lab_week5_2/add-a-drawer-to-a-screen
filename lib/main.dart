import 'package:flutter/material.dart';

import 'widgets/item_screen1.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  static const appTitle = 'Drawer Demo';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomepage(title: appTitle),
    );
  }
}

class MyHomepage extends StatefulWidget {
  MyHomepage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  MyHomepageState createState() => MyHomepageState(title: title);
}

class MyHomepageState extends State<MyHomepage> {
  MyHomepageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('My Page!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: body,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Drawer Header'),
            ),
            ListTile(
              title: const Text('Item 1'),
              onTap: () {
                setState(() {
                  body = ItemScreen();
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Item 2'),
              onTap: () {
                setState(() {
                  body = Center(
                    child: Text('Item 2'),
                  );
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Item 3'),
             onTap: () {
                setState(() {
                  body = Center(
                    child: Text('Item 3'),
                  );
                });
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}



